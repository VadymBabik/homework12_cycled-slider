"use strict";
const list = document.querySelectorAll("img");
let showlist = 0;
let sliderWorks = false;

function slider() {
  list[showlist].className = "image-to-show";
  showlist = (showlist + 1) % list.length;
  list[showlist].classList.add("show");
}

let sliderShow = setInterval(slider, 3000, (sliderWorks = true));

const stop = document.querySelector(".stop");
stop.addEventListener("click", pause);

function pause() {
  clearInterval(sliderShow);
  sliderWorks = false;
}

const listGo = document.querySelector(".next");
listGo.addEventListener("click", next);

function next() {
  if (!sliderWorks) {
    sliderShow = setInterval(slider, 3000, (sliderWorks = true));
  }
}
